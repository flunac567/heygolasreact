import React from "react";
import { StyleSheet,Text,View } from "react-native";
import colors from "../Colors";

export default ListaTareas=({list})=>{
    const contarCompletas=list.tareas.filter(tareas=> tareas.completed).length;
    const contarPendientes= list.tareas.length-contarCompletas;

    return(
        <View style={[styles.lisContainer, {backgroundColor: list.color},]}>
            <Text style={styles.listTitles} numberOfLines={1}>
                {list.nombre}
                </Text>

                <View>
                    <View style={{alignItems:"center"}}>
                       <Text style={styles.count}>{contarCompletas}</Text>
                        <Text style={styles.subtitle}>Completadas</Text>
                    </View>
                     
                    <View style={{alignItems:"center"}}>
                        <Text style={styles.count}>{contarPendientes}</Text>
                        <Text style={styles.subtitle}>Pendientes</Text>
                    </View>

                </View>
        </View>
    );
};


const styles=StyleSheet.create({
    lisContainer: {
        paddingVertical:32,
        paddingHorizontal:16,
        borderRadius:6,
        marginHorizontal:12,
        alignItems:"center",
        width:200
    },
    listTitles:{
        fontSize:24,
        fontWeight:"700",
        color: colors.Blanco,
        marginBottom:18
    },
    count:{
        fontSize:40,
        fontWeight:"200",
        color:colors.Blanco
    },
    subtitle:{
        fontSize:12,
        fontWeight:"700",
        color:colors.Blanco
    }
})