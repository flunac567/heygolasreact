import React from "react";
import { StyleSheet,Text,View,  KeyboardAvoidingView,TouchableOpacity,TextInput } from "react-native";
import colors from "../Colors";
import { AntDesign } from "@expo/vector-icons";
import tempData from "../tempData";

export default class AñadirListaModal extends   React.Component{
    coloresDeFondo=["#420343","#1766ce","#1422f0","#f30cf3","#00ec5b"]
    state={
        nombre:"",
        color:this.coloresDeFondo[0]
    };

    crearTarea=()=>{
        const {nombre,color}=this.state

        tempData.push({
            nombre,
            color,
            tareas:[]
        })

        this.setState({nombre:""})
        this.props.closeModal()
    }

    renderColors(){
        return this.coloresDeFondo.map(color=>{
            return(
                <TouchableOpacity
                 key={color}
                 style={[styles.colorSelected,{backgroundColor:color}]}
                 onPress={()=>this.setState({color})}
                />
            );
        });
    }

    render(){
        return(
            <KeyboardAvoidingView style={styles.container} behavior="padding">
                    <TouchableOpacity style={{position:"absolute", top:64,right:32}} onPress={this.props.closeModal }>
                      <AntDesign name="close" size={24} color={colors.Negro}></AntDesign>
                    </TouchableOpacity>

                    <View style={{alignItems:"stretch",marginHorizontal:32}}>
                        <Text style={styles.title}>Crea un nuevo habito</Text>
                        <TextInput 
                        style={styles.input} 
                        placeholder="Nombre de tu habito"
                        onChangeText={text=>this.setState({nombre:text})}
                        />
                        
                        <View style={{flexDirection:"row",justifyContent:"space-between",marginTop:10}}>{this.renderColors()}</View>

                        <TouchableOpacity style={[styles.create,{backgroundColor:this.state.color}]} onPress={this.crearTarea}>
                        <Text style={{color:colors.Blanco,fontWeight:"600"}}>Crear</Text>
                        </TouchableOpacity>

                           
                    </View>
            </KeyboardAvoidingView>
        );
    }
}

const styles= StyleSheet.create({
    container:{
            flex:1,
            justifyContent:"center",
            alignItems:"center"
    },
    title:{
        fontSize:28,
        fontWeight:"800",
        alignSelf:"center",
        marginBottom:16
    },
    input:{
        borderWidth:StyleSheet.hairlineWidth,
        borderColor:colors.Azul,
        borderRadius:6,
        height:58,
        paddingHorizontal:16,
        fontSize:18
    },
    create:{
        marginTop:24,
        height:58,
        borderRadius:6,
        alignItems:"center",
        justifyContent:"center"
    },
    colorSelected:{
        width:32,
        height:32,
        borderRadius:4
    }
})