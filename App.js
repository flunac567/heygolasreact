import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View,FlatList,Modal } from 'react-native';
import { AntDesign } from "@expo/vector-icons";
import colors  from "./Colors";
import ListaTareas from './components/ListaTareas';
import tempData from "./tempData";
import AñadirListaModal from "./components/añadirListaModal";
export default class App extends React.Component {

  state={
    añadirTareaVisible:false
  };

  toggleAñadirModal(){
    this.setState({añadirTareaVisible: !this.state.añadirTareaVisible});
  }


  render() {
    return (
      <View style={styles.container}>

        <Modal 
        animationType="slide" 
        visible={this.state.añadirTareaVisible}
        onRequestClose={()=>this.toggleAñadirModal()}>
          <AñadirListaModal closeModal={()=>this.toggleAñadirModal()}/>
        </Modal>

        <View style={{flexDirection: "row"}}>
          <View style={styles.divider}/>
          <Text style={styles.title}>
            Hey! <Text style={{ fontWeight:"300", color: colors.Azul}}>Goals</Text>
          </Text>
          <View style={styles.divider}/>
          </View>

          <View style={{marginVertical: 48 }}>
             <TouchableOpacity style={ styles.añadirLista } onPress={()=>this.toggleAñadirModal()}>
               <AntDesign name="plus" size={16} color={colors.Azul}></AntDesign>
             </TouchableOpacity>

             <Text style={styles.añadir}>Añadir Lista</Text>
          </View>
        
          <View style={{height:275,paddingLeft:32}}>
           <FlatList data={tempData}
           keyExtractor={item=>item.nombre}
           horizontal={true}
           showsHorizontalScrollIndicator={false}
           renderItem={({item})=><ListaTareas list={item}/> }
           />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  divider: {
    backgroundColor: colors.BlanquiAzul,
    height:1,
    flex:1,
    width:2,
    alignSelf:"center"
  },
  title:{
    fontSize:38,
    fontWeight:"800",
    paddingHorizontal:14,
    color:colors.Negro
  },
  añadirLista:{
      borderWidth:2,
      borderColor: colors.BlanquiAzul,
      borderRadius:4,
      padding:16,
      alignItems:"center",
      justifyContent:"center"
  },
  añadir: {
    color:colors.Azul,
    fontWeight:"600",
    fontSize:14,
    marginTop:8
  }
});
