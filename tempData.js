export default tempData=[
    {
      "userId": 1,
      "id": 1,
      "nombre": "Mañana",
      "color": "#1422f0",
      "tareas":[
        {
          "title":"Clase de Zoom",
            "completed": true      
        },
        {
          "title":"Pasear al perro",
            "completed": true      
        },
        {
          "title":"Hacer las compras",
            "completed": true      
        },
        {
          "title":"Hacer las compras",
            "completed": false    
        }
      ]
      
      
    },
    {
      "userId": 1,
      "id": 2,
      "nombre": "Tarde",
       "color":"#1cce8a",
       "tareas":[
        {
          "title":"Hacer la tesis",
            "completed": true      
        },
        {
          "title":"Clase de Ingles",
            "completed": false      
        }
       ]
      
     
    },
    {
      "userId": 1,
      "id": 3,
      "nombre": "Noche",
       "color":"#f3c90c",
       "tareas":[
         {
           "title":"Jugar crash",
            "completed": false       
          },
          {
            "title":"Preparar todo para mañana",
              "completed":false      
          },
          {
            "title":"Dormir",
              "completed": false      
          }
       ]
      
     
    },
    {
      "userId": 1,
      "id": 4,
      "nombre": "Proxima",
       "color":"#0cd0f3",
       "tareas":[
         {
           "title":"Tomar el micro",
             "completed": false      
          },
          {
            "title":"Desayunar",
              "completed": false      
          },
       ]
     
     
    }
  ]