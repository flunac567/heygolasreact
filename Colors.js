import { NativeModules } from "react-native";

export default colors={
    Negro:"#420343",
    Azul:"#1766ce",
    BlanquiAzul:"#1422f0",
    Blanco:"#FFFFFF"
};
